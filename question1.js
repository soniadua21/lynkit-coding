let promise_a = new Promise(function (resolve, reject) {
    return resolve('Promise A')
});
let promise_b = new Promise(function (resolve, reject) {
    return resolve('Promise B')
});
let promise_c = new Promise(function (resolve, reject) {
    return resolve('Promise C')
});
let promise_d = new Promise(function (resolve, reject) {
    return resolve('Promise D')
});
let promise_e = new Promise(function (resolve, reject) {
    return resolve('Promise E')
});


function ques_promises() {
    promise_a.then((p1) => {
        console.log(p1)
        Promise.all([promise_b, promise_c]).then((p2) => {
            console.log(p2)
            callPromiseD();
        }).catch("err")
    }).catch("err");
}

function callPromiseD() {

    promise_d.then((p4) => {
        console.log(p4);
        promise_e.then((p5) => console.log(p5)).catch(e => callPromiseD());
    })
}

ques_promises()