//Write a program to read data from database (mongodb [mongoose] ) having nested loop


//Not clear about the question but implemet according to understanding
  
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var Category = db.collection('Category');
var Products = db.collection('Products');
MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    var dbo = db.db("mydb");
    dbo.collection("Category").find({}).toArray(function (err, result) {
        if (err) throw err;
        for (let i = 0; result.length; i++) {
            Products.find({}).toArray(function (err, result) {
                if (err) throw err;
                console.log(result);
                db.close();
            });
        }
        console.log(result);
        db.close();
    });
});



// Write a program to demonstrate mongoose pre hook save.

catSchema.pre("save", (next) => {
    this.categoryName = 'Chinese'
    this.categoryCod = 'CategoryCode1'
    this.Product.push({
        productType: 'Burger',
        productCode: 'BG1',
        productDetails: productDetails.push({
            productName: 'Veg Burger',
            productCost: '$26',
            productDescription: 'Mixture of different veggies'
        })
    })
    next()
})


//Design a schema in mongoose for : [ Category, Product, Product _Details]


var Product_Details = new mongoose.Schema({
    productName: {
        type: String,
        required: "Required"
    },
    productCost: {
        type: String
    },
    productDescription: {
        type: String
    },

})

var Product = new mongoose.Schema({
    productType: {
        type: String,
        required: "Required"
    },
    productCode: {
        type: String
    },
    productDetails: [Product_Details]
})

var Category = new mongoose.Schema({
    categoryName: {
        type: String,
        required: "Required"
    },
    categoryCod: {
        type: String
    },
    Product: [Product]
})

var catSchema = mongoose.model('Category', Category)